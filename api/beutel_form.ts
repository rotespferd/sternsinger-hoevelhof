import { NowRequest, NowResponse } from '@vercel/node'


export default function (req: NowRequest, res: NowResponse) {
    const { name, phone, numberOfTimes } = req.body
    console.log(name);
    console.log(phone);
    console.log(numberOfTimes);

    sendMail(name, phone, numberOfTimes).catch(console.error);
    console.log("Mail send");

    res.redirect("/");
    //res.send(`Hello!`);
}

async function sendMail(name, phone, numberOfTimes) {
    var messageHtml  = "Neue Formular-Anmeldung<br>";

    messageHtml += `Name: ${name}<br>`;
    messageHtml += `Telefonnummer: ${phone}<br>`;
    messageHtml += `Wie häufig war ich dabei: ${numberOfTimes}`;

    const nodemailer = require("nodemailer");

    const user = process.env.MAIL_USERNAME;
    const password = process.env.MAIL_PASSWORD; 
    

    let transporter = nodemailer.createTransport({
        host: "smtp.office365.com",
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
          user: user, // generated ethereal user
          pass: password, // generated ethereal password
        },
    });

    let info = await transporter.sendMail({
        from: 'marian@marianhahne.de', // sender address
        to: "beutel@sternsinger-hoevelhof.de", // list of receivers
        subject: "Neue Beutel-Anmeldung", // Subject line
        html: messageHtml, // html body
    });

    console.log(info);
}